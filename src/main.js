import { createApp } from "vue/dist/vue.esm-bundler";

import AppPage from "./modules/app/pages/AppPage.vue";
import { createRedux } from "./providers/reduxProvider";
import { store } from "./providers/store";
import { createVfm } from "vue-final-modal";
import { router } from "./providers/router";

import "vue-final-modal/style.css";
import "./assets/main.css";

const app = createApp(AppPage);
const vfm = createVfm();

app.use(createRedux(store));
app.use(router);
app.use(vfm);
app.mount("#app");
