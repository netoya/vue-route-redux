import { createAsyncThunk } from "@reduxjs/toolkit";

export const fetchLogin = createAsyncThunk(
  "login/login",
  async ({ user, pass }, { rejectWithValue }) => {
    console.log("Thunk:fetchLogin", { user, pass });
    await new Promise((resolve) => setTimeout(() => resolve(1), 4000));

    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/pikachu`);
    const data = await response.json();
    if (response.status < 200 || response.status >= 300) {
      return rejectWithValue(data);
    }
    console.log({ data });
    return { token: 1234 };
  }
);
