import { createSlice } from "@reduxjs/toolkit";
import { fetchLogin } from "./loginThunks";

export const loginSlice = createSlice({
  name: "login",
  initialState: {
    token: "",
    loginLoading: false,
  },
  reducers: {
    logout: (state) => {
      state.token = "";
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchLogin.pending, (state, action) => {
      state.loginLoading = true;
    });

    builder.addCase(fetchLogin.fulfilled, (state, action) => {
      state.token = action.payload.token;
      state.loginLoading = false;
    });

    builder.addCase(fetchLogin.rejected, (state, action) => {
      state.loginLoading = false;
    });
  },
});

export const loginActions = { ...loginSlice.actions, login: fetchLogin };
export const loginReducer = loginSlice.reducer;
