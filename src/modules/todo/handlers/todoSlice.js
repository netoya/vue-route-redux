import { createSlice } from "@reduxjs/toolkit";

export const todoSlice = createSlice({
  name: "todo",
  initialState: {
    todoList: [],
    open: true,
  },
  reducers: {
    toggleModal: (state) => {
      state.open = !state.open;
    },
    addTask: (state, action) => {
      state.todoList.push(action.payload);
    },
    removeTodo: (state) => {
      state.todoList.pop();
    },
  },
});

export const todoActions = todoSlice.actions;
export const todoReducer = todoSlice.reducer;
