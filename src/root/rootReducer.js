import { todoReducer } from "../modules/todo/handlers/todoSlice";
import { loginReducer } from "../modules/login/handlers/loginSlice";

export const rootReducer = {
  todos: todoReducer,
  login: loginReducer,
};
