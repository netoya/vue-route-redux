import { todoRoutes } from "../modules/todo/handlers/todoRoutes.js";

const Home = { template: "<div>Home</div>" };
const About = { template: "<div>About</div>" };

export const rootRoutes = [
  { path: "/", component: Home },
  { path: "/about", component: About },
  ...todoRoutes,
];
