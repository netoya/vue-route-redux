import { inject, computed } from "vue";
import { storeKey } from "./reduxProvider";
import { store } from "./store";

export const useDispath = () => store.dispatch;

export const useSelector = (fn) => {
  const rootStore = inject(storeKey);
  return computed(() => fn(rootStore.state));
};
