// store.js
import { configureStore } from "@reduxjs/toolkit";
import { rootReducer } from "../root/rootReducer";

export const store = configureStore({
  reducer: rootReducer,
});
