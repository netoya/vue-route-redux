import { createRouter, createWebHashHistory } from "vue-router";
import { rootRoutes } from "../root/rootRoutes";

export const router = createRouter({
  history: createWebHashHistory(),
  routes: rootRoutes,
});
